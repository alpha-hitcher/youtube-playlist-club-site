<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
	Route::put('playlist', ['as' => 'playlist.create', 'uses' => 'PlaylistController@store']);
	Route::put('playlist/changeState', ['as' => 'playlist.changeState', 'uses' => 'PlaylistController@changeState']);
    Route::put('/playlist/{playlist_id?}', ['as'=> 'playlist.comment', 'uses' => 'PlaylistController@comment']);
    Route::delete('/playlist/deleteFavor/{playlist_id?}', ['as' => 'playlist.deleteFavor', 'uses' => 'PlaylistController@deleteFavor']);
    Route::post('/getPlaylistAjax', 'PlaylistController@getPlaylist');
    Route::put('/playlist/edit/{playlist_id?}', ['as' =>'playlist.edit', 'uses' => 'PlaylistController@editPlaylist']);
    Route::put('/ads', ['as' =>'ad', 'uses' =>'PlaylistController@adverse']);
    Route::get('/managead', ['as' =>'manage.ad', 'uses' => 'HomeController@adverse']);
    Route::put('/publishad', ['as' =>'publish.ad', 'uses' => 'HomeController@publishAds']);

    Route::put('/contact', ['as' => 'contact', 'uses' => 'HomeController@contact']);
    Route::put('/contact/check', ['as' => 'contact.state', 'uses' => 'HomeController@checkContact']);

    Route::put('/upload', ['as' => 'upload', 'uses' => 'ProfileController@upload']);
});

Route::put('/', ['as' =>'home.getCategory', 'uses' =>'HomeController@getCategory']);
Route::post('/', ['as' =>'home.getPlaylist', 'uses' =>'HomeController@getPlaylist']);
Route::get('/{id?}', 'HomeController@reindex') ->name('home');
Route::get('/playlist/newest', ['as' =>'home.newest', 'uses' =>'HomeController@newest']);
Route::get('/playlist/{id}', ['as' =>'home.video', 'uses' => 'HomeController@video']);
Route::post('/action', ['as' => 'home.action', 'uses' => 'HomeController@action']);
Route::get('/contact/get', ['as' => 'contact.get', 'uses' => 'HomeController@getContact']);


