<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlaylistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('playlist', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('created_by')->comment('Creator ID');
            $table->integer('category')->comment('Category ID');
            $table->boolean('display_state') ->default(true) ->comment('Publish/Private');
            $table->integer('liked') ->default(0)-> comment('Like number');
            $table->integer('dislike') ->default(0) ->comment('Dislike number');
            $table->integer('parent')->default(1) ->comment('playlist->1, video ->2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('playlist');
    }
}
