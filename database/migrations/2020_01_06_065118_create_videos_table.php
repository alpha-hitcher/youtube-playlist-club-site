<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('url') ->comment('Youtube video url');
            $table->string('embed_code') ->comment('Youtube video embed code');
            $table->string('thumbnail') ->comment('Video thumbnail from youtube');
            $table->unsignedInteger('playlist') ->comment('playlist or video id.');
            $table->integer('liked')->default(0)->comment('Like number');
            $table->integer('dislike')->default(0)->comment('Dislike number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
