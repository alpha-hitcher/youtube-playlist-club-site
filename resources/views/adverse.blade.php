@extends('layouts.youtube')

@section('content')
    @include('layouts.headers.profileheader')

    <div class="modal fade" id="advertisement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Input your advertise url here.</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action = "{{route('ad')}}">
                    @csrf
                    @method('put')
                    <div class="modal-body">
                        <input type="text" id="advertisement_input" style="width:100%;" name="url" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send URL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 col-md-12 col-12 mb-5 mb-xl-0">
                <table class="table align-items-center table-dark">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Video</th>
                        <th scope="col">URL</th>
                        <th scope="col">PUBLISH/HIDE</th>
                        <th scope="col">APPLIED AT</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($ads as $ad)
                        <tr>
                            <th scope="row">
                                <div class="media align-items-center">
                                    <a href="" class="mr-3" target="_blank">
                                        <img src="{{$ad -> thumbnail }}" style="width:120px;height:80px;">
                                    </a>
                                </div>
                            </th>
                            <td>
                                <button type="button" class="btn btn-warning btn-sm">{{ $ad ->url }}</button>
                            </td>
                            <td>
                                <form method="post" action="{{route('publish.ad')}}" >
                                    @csrf
                                    @method('put')
                                    <input type="hidden" name="id" value="{{$ad->id}}">
                                    <label class="custom-toggle">
                                        <input name="state" type="checkbox"   @if($ad->state) checked @endif
                                        onchange="event.preventDefault();this.parentNode.parentNode.submit();">
                                        <span class="custom-toggle-slider rounded-circle"></span>
                                    </label>
                                </form>
                            </td>
                            <td>{{$ad ->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection