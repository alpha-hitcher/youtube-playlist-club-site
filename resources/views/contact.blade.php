@extends('layouts.youtube')

@section('content')
    @include('layouts.headers.profileheader')

    <div class="modal fade" id="advertisement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Input your advertise url here.</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action = "{{route('ad')}}">
                    @csrf
                    @method('put')
                    <div class="modal-body">
                        <input type="text" id="advertisement_input" style="width:100%;" name="url" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send URL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 col-md-12 col-12 mb-5 mb-xl-0">
                <table class="table align-items-center table-dark">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">NO</th>
                        <th scope="col">NAME</th>
                        <th scope="col">EMAIL</th>
                        <th scope="col">SUBJECT</th>
                        <th scope="col">CHECK STATE</th>
                        <th scope="col">APPLY DATE</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($contacts as $contact)
                        <tr>
                            <th scope="row">
                                {{$contact -> id }}
                            </th>
                            <td>
                                {{ $contact-> fullname }}
                            </td>
                            <td>
                                {{ $contact-> email }}
                            </td>
                            <td>
                                {{ $contact-> subject }}
                            </td>
                            <td>
                                <form method="post" action="{{route('contact.state')}}" >
                                    @csrf
                                    @method('put')
                                    <input type="hidden" name="id" value="{{$contact->id}}">
                                    <label class="custom-toggle">
                                        <input name="state" type="checkbox"   @if($contact->state) checked @endif
                                        onchange="event.preventDefault();this.parentNode.parentNode.submit();">
                                        <span class="custom-toggle-slider rounded-circle"></span>
                                    </label>
                                </form>
                            </td>
                            <td>{{$contact ->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection