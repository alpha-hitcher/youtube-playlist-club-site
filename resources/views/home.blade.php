@extends('layouts.youtube')

@section('content')
    @include('layouts.headers.cards')

    <div class="modal fade" id="share_link" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Share Link</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" id="share_link_input" style="width:100%;">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="copyLinkToClipboard();">Copy Link</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="comment" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
            <div class="modal-dialog modal- modal-dialog-centered modal- modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="modal-title-default">Comments</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body comment-modal-body">

                    </div>

                    <div class="modal-footer">
                        <form class="comment-form" action="{{route('playlist.comment')}}" method="post" id="comment-form">
                            @csrf
                            @method('put')
                            <textarea class="form-control"  name="comment" rows="2" placeholder="Add a comment here ..." required></textarea>
                        </form>
                        <button type="button" class="btn btn-primary" onclick="event.preventDefault();document.getElementById('comment-form').submit();">Comment</button>
                        <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>

    <div class="modal fade" id="advertisement" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Input your advertise url here.</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action = "{{route('ad')}}">
                    @csrf
                    @method('put')
                    <div class="modal-body">
                        <input type="text" id="advertisement_input" style="width:100%;" name="url" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send URL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Contact us for Advertisements</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{route('contact')}}">
                    @csrf
                    @method('put')
                    <div class="modal-body contact">

                                <label for="name">Full Name</label>
                                <input type="text" id="name" name="fullname" placeholder="John Doe" required>

                                <label for="email">Email</label>
                                <input type="email" id="email" name="email" placeholder="example@domain.com" required>

                                <label for="subject">Subject</label>
                                <textarea id="subject" name="subject" placeholder="Your contact here..." style="height:100px" required></textarea>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-9 col-md-9 col-12 mb-5 mb-xl-0">
                <div class="table-responsive">
                    <table class="table align-items-center table-dark">
                        <thead>
                        <tr>
                            <th scope="col">TOPIC</th>
                            <th scope="col">CATEGORY</th>
                            <th scope="col">VIDEO NUMBER</th>
                            <th scope="col">INFO</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody id="playlist-box">
                        @foreach($playlists as $playlist)
                            <tr>
                                <th scope="row">
                                    <div class="media align-items-center">
                                        <a href="{{route('home.video', ['id' => $playlist->id])}}" class=" mr-3" target="_blank">
                                            <img src="{{$playlist->top_video->thumbnail}}" style="width:120px;height:80px;">
                                            <span class="media-body">
                                        <span class="mb-0 text-sm">{{ $playlist ->name }}</span>
                                    </span>
                                        </a>
                                    </div>
                                </th>
                                <td><button type="button" class="btn btn-warning btn-sm">{{$playlist ->category_name}}</button></td>
                                <td>
                                    <button class="btn btn-icon btn-2 btn-primary" type="button">
                                        {{$playlist ->total_videos}}
                                    </button>
                                </td>
                                <td>
                                    <button class="btn btn-icon btn-2 btn-danger action" type="button" data-toggle="tooltip" data-placement="top" title="Like" data-score = "liked:playlist:{{$playlist->id}}" onclick="action(this)">
                                        👍<i>{{$playlist->liked}}</i>
                                    </button>
                                    <button class="btn btn-icon btn-2 btn-info action" type="button" data-toggle="tooltip" data-placement="top" title="Dislike" data-score = "dislike:playlist:{{$playlist->id}}" onclick="action(this)">
                                        👎<i>{{$playlist->dislike}}</i>
                                    </button>
                                    <button type="button" class="btn btn-primary action" data-toggle="modal" data-target="#comment"  data-score = "comment:playlist:{{$playlist->id}}" onclick="action(this)">
                                        💬<i>{{$playlist->comment}}</i>
                                    </button>
                                </td>
                                <td class="text-right">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item share" data-toggle="modal" data-target="#share_link" data-score = "{{route('home.video',['id' =>$playlist->id])}}" onclick="share(this)"><i class="fa fa-share"></i>Share</a>
                                            <a class="dropdown-item action" data-toggle="modal" data-score = "favor:playlist:{{$playlist->id}}"  onclick="action(this)"><i class="ni ni-fat-add"></i> Add to Favorite</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="col-xl-3 col-md-3 col-0">
                <div class="card shadow">
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                            <div class="col">
                                <h2 class="mb-0">Advertisements</h2>
                                <a data-toggle="modal" data-target = "#contact" href=""><h5 class="text-uppercase text-muted ls-1 mb-1">Contact us for Advertisements</h5> here !</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ads">
                        @foreach($ads as $ad)
                            <?php echo $ad->embed_code;?>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection