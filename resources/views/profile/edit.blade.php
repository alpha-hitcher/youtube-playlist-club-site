@extends('layouts.youtube', ['title' => __('User Profile')])

@section('content')
    @include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('This is your profile page. You can edit your profile and create, edit , manage your playlists, favourite lists here.'),
        'class' => 'col-lg-7'
    ])

    <div class="modal fade" id="edit-profile" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
        <div class="modal-dialog modal- modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <div class="card bg-secondary shadow">
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <h3 class="col-12 mb-0">{{ __('Edit Profile') }}</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <form method="post" action="{{ route('profile.update') }}" autocomplete="off">
                                @csrf
                                @method('put')

                                <h6 class="heading-small text-muted mb-4">{{ __('User information') }}</h6>
                                <div class="pl-lg-4">
                                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                        <input type="text" name="name" id="input-name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Name') }}" value="{{ old('name', auth()->user()->name) }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-email">{{ __('Email') }}</label>
                                        <input type="email" name="email" id="input-email" class="form-control form-control-alternative{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" value="{{ old('email', auth()->user()->email) }}" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                    </div>
                                </div>
                            </form>
                            <hr class="my-4" />
                            <form method="post" action="{{ route('profile.password') }}" autocomplete="off">
                                @csrf
                                @method('put')

                                <h6 class="heading-small text-muted mb-4">{{ __('Password') }}</h6>

                                @if (session('password_status'))
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        {{ session('password_status') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif

                                <div class="pl-lg-4">
                                    <div class="form-group{{ $errors->has('old_password') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-current-password">{{ __('Current Password') }}</label>
                                        <input type="password" name="old_password" id="input-current-password" class="form-control form-control-alternative{{ $errors->has('old_password') ? ' is-invalid' : '' }}" placeholder="{{ __('Current Password') }}" value="" required>

                                        @if ($errors->has('old_password'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-password">{{ __('New Password') }}</label>
                                        <input type="password" name="password" id="input-password" class="form-control form-control-alternative{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('New Password') }}" value="" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-password-confirmation">{{ __('Confirm New Password') }}</label>
                                        <input type="password" name="password_confirmation" id="input-password-confirmation" class="form-control form-control-alternative" placeholder="{{ __('Confirm New Password') }}" value="" required>
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success mt-4">{{ __('Change password') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="share_link" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Share Link</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" id="share_link_input" style="width:100%;">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="copyLinkToClipboard();">Copy Link</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
                <div class="card card-profile shadow">
                    <div class="row justify-content-center">
                        <div class="col-lg-3 order-lg-2">
                            <div class="card-profile-image">
                                    <img id="profile-image" src="{{ asset('storage') }}/image/avatar/{{auth()->user()->avatar}}" class="rounded-circle" style="cursor: pointer;">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0 pt-md-4" style="margin-top: 7em;">
                        <form action="{{route('upload')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <input type="file" accept=".jpg,.jpeg,.png" style="display:none" onchange="readURL(this)" name="profile_image">
                            <input type="submit" class="btn btn-icon btn-2 btn-primary" value="save" style="display:none" id="file">
                        </form>
                        <div class="text-center">
                            <h3>
                                {{ auth()->user()->name }}<br>
                                {{ auth()->user()->email }}
                            </h3>
                            <div class="h5 font-weight-300">
                                <i class="ni location_pin mr-2"></i>{{ __('Member since '.auth()->user()->created_at) }}
                            </div>
                            <button type="button" class="btn btn-block btn-default" data-toggle="modal" data-target="#edit-profile">{{ __('Edit Profile') }}</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 order-xl-1">
                <div class="nav-wrapper">
                    <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-create-playlist" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>{{ __('New Playlist') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-my-playlist" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-button-play mr-2"></i>{{ __('My Playlists') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-favor-playlist" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i class="ni ni-favourite-28 mr-2"></i>{{ __('My Favourite Playlists') }}</a>
                        </li>
                        <li class="nav-item" style="display:none">
                            <a class="nav-link mb-sm-3 mb-md-0" data-toggle="tab" href="#tabs-edit-playlist" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false" id="tab_button"><i class="ni ni-favourite-28 mr-2"></i>{{ __('Edit Playlists') }}</a>
                        </li>
                    </ul>
                </div>
                <div class="card shadow">
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="tab-content">
                            <div class="tab-pane fade" id="tabs-create-playlist" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                                <div class="card-header bg-white border-0">
                                    <div class="row align-items-center">
                                        <h3 class="col-12 mb-0">{{ __('Create Youtube Playlist') }}</h3>
                                    </div>
                                </div>
                                <form method="post" action="{{ route('playlist.create') }}" autocomplete="off">
                                    @csrf
                                    @method('put')
                                    <div class="pl-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="playlist">{{ __('Topic') }}</label>
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Type here playlist name" required>

                                        </div>
                                        <div class="form-group">
                                            <label class="form-control-label" for="category">{{ __('Category') }}</label>
                                            <input id="category" name="category" type="text" required>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-control-label" for="video">{{ __('URL') }}</label>
                                            <input type="text" class="form-control" id="url" name="url" placeholder="" required>
                                        </div>

                                        <div class="text-center">
                                            <button type="submit" class="btn btn-success mt-4">{{ __('Create Playlist') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade  show active" id="tabs-my-playlist" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                                <div class="table-responsive">
                                    <table class="table align-items-center">
                                        <thead class="thead-light">
                                        <tr>
                                            <th scope="col">Topic</th>
                                            <th scope="col">Category</th>
                                            <th scope="col">Video Num</th>
                                            <th scope="col">Publish/Private</th>
                                            <th scope="col">Info</th>
                                            <th scope="col"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($playlists as $playlist)
                                        <tr>
                                            <th scope="row">
                                                <div class="media align-items-center">
                                                    <a href="{{route('home.video', ['id' => $playlist->id])}}" class="mr-3" target="_blank">
                                                        <img src="{{$playlist->top_video->thumbnail}}" style="width:120px;height:80px;">
                                                        <span class="media-body">
                                                            <span class="mb-0 text-sm">{{$playlist ->name }}</span>
                                                        </span>
                                                    </a>
                                                </div>
                                            </th>
                                            <td>
                                                <button type="button" class="btn btn-warning btn-sm">{{ $playlist ->category_name }}</button>
                                            </td>
                                            <td><button class="btn btn-icon btn-2 btn-primary" type="button">{{$playlist->total_videos}}</button></td>
                                            <td>
                                                <form method="post" action="{{route('playlist.changeState')}}" >
                                                    @csrf
                                                    @method('put')
                                                    <input type="hidden" name="id" value="{{$playlist->id}}">
                                                <label class="custom-toggle">
                                                    <input name="state" type="checkbox"   @if($playlist->display_state) checked @endif
                                                    onchange="event.preventDefault();this.parentNode.parentNode.submit();">
                                                    <span class="custom-toggle-slider rounded-circle"></span>
                                                </label>
                                                </form>
                                            </td>
                                            <td>
                                                <div class="card-body">
                                                    <span class="mt-3 mb-0 text-muted text-sm">
                                                        <span class="text-danger">👍{{$playlist->liked}}</span>
                                                    </span>
                                                    <span class="mt-3 mb-0 text-muted text-sm">
                                                        <span class="text-default">👎{{$playlist->dislike}}</span>
                                                    </span>
                                                    <span class="mt-3 mb-0 text-muted text-sm">
                                                        <span class="text-success">💬{{$playlist->comment}}</span>
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="text-right">
                                                <div class="dropdown">
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item"  onclick="edit($(this));" data-score = "{{$playlist ->id}}">✍🏽Edit</a>
                                                        <a class="dropdown-item share" data-toggle="modal" data-target = "#share_link" data-score = "{{route('home.video',['id' =>$playlist->id])}}" onclick="share(this)">
                                                            <i class="fa fa-share"></i> Share
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs-favor-playlist" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                                <div class="table-responsive">
                                    <table class="table align-items-center">
                                        <thead class="thead-light">
                                        <tr>
                                            <th scope="col">TOPIC</th>
                                            <th scope="col">CATEGORY</th>
                                            <th scope="col">VIDEO NUM</th>
                                            <th scope="col">INFO</th>
                                            <th scope="col"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($favors as $favor)
                                        <tr>
                                            <th scope="row">
                                                <div class="media align-items-center">
                                                    <a href="{{route('home.video', ['id' => $favor->id])}}" class=" mr-3">
                                                        <img src="{{$favor->top_video->thumbnail}}" style="width:120px;height:80px;">
                                                        <span class="media-body">
                                                            <span class="mb-0 text-sm">{{$favor->name}}</span>
                                                        </span>
                                                    </a>
                                                </div>
                                            </th>
                                            <td><button type="button" class="btn btn-warning btn-sm">{{ $favor ->category_name }}</button></td>
                                            <td><button class="btn btn-icon btn-2 btn-primary" type="button">{{$favor->total_videos}}</button></td>
                                            <td>
                                                <div class="card-body">
                                                    <span class="mt-3 mb-0 text-muted text-sm">
                                                        <span class="text-danger">👍{{$favor->liked}}</span>
                                                    </span>
                                                    <span class="mt-3 mb-0 text-muted text-sm">
                                                        <span class="text-default">👎{{$favor->dislike}}</span>
                                                    </span>
                                                    <span class="mt-3 mb-0 text-muted text-sm">
                                                        <span class="text-success">💬{{$favor->comment}}</span>
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="text-right">
                                                <div class="dropdown">
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item share" data-toggle="modal" data-target = "#share_link" data-score = "{{route('home.video',['id' =>$favor->id])}}" onclick="share(this)">
                                                           <i class="fa fa-share"></i>Share
                                                        </a>
                                                        <a class="dropdown-item" onclick="deleteFavor($(this));"><form action="{{route('playlist.deleteFavor')}}/{{$favor->id}}" method="post">
                                                        @csrf
                                                        @method('delete')
                                                        </form><i class="ni ni-fat-remove"></i> Delete from Favorite</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tabs-edit-playlist" role="tabpanel" aria-labelledby="tabs-icons-text-4-tab">
                                <div class="card-header bg-white border-0">
                                    <div class="row align-items-center">
                                        <h3 class="col-12 mb-0">{{ __('Edit Playlist') }}</h3>
                                    </div>
                                </div>
                                <form method="post" action="" autocomplete="off" id="playlist_edit_form">
                                    @csrf
                                    @method('put')
                                    <div class="pl-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name_edit">{{ __('Topic') }}</label>
                                            <input type="text" class="form-control" id="name_edit" name="name" placeholder="Type here playlist name" required>

                                        </div>
                                        <div class="form-group">
                                            <label class="form-control-label" for="category_edit">{{ __('Category') }}</label>
                                            <input id="category_edit" name="category" type="text" class="form-control" readonly>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-control-label" for="url_edit">{{ __('URL') }}</label>
                                            <input type="text" class="form-control" id="url_edit" name="url" placeholder="" required>
                                        </div>

                                        <div class="text-center">
                                            <button type="submit" class="btn btn-success mt-4">{{ __('Edit Playlist') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection