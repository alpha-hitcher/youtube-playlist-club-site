<nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
    <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="{{ route('home') }}">{{ __('Dashboard') }}</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-light form-inline mr-3 d-none d-md-flex">
            <div class="form-group mb-0">
                <h1 style="color:white">Youtube PlayList Club</h1>
            </div>
        </form>
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
            <li class="nav-item dropdown">
                <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                            <img alt="Image placeholder" src="{{ asset('argon') }}/img/avatar/default.jpg">
                        </span>
                        <div class="media-body ml-2 d-none d-lg-block">
                            <span class="mb-0 text-sm  font-weight-bold">
                                @auth()
                                    {{ auth()->user()->name }}
                                @endauth
                                    @guest()
                                    Welcome
                                    @endguest
                            </span>
                        </div>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{ __('Welcome!') }}</h6>
                    </div>
                    <a href="{{ route('login') }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>{{ __('Login') }}</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('register') }}" class="dropdown-item">
                        <i class="ni ni-user-run"></i>
                        <span>{{ __('Register') }}</span>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</nav>