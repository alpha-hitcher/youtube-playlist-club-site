<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="{{ route('home') }}">
            <img src="{{ asset('argon') }}/img/brand/youtube-logo.png" class="navbar-brand-img" alt="...">
            PlayLists Club
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                            @auth()
                        <img alt="Image placeholder" src="{{ asset('storage') }}/image/avatar/{{auth()->user()->avatar}}">
                            @endauth
                            @guest()
                                    <img alt="Image placeholder" src="{{ asset('argon') }}/img/avatar/default.jpg">
                                @endguest
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{ __('Welcome!') }}</h6>
                    </div>
                    <a href="{{ route('profile.edit') }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>{{ __('My profile') }}</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="ni ni-user-run"></i>
                        <span>{{ __('Logout') }}</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('argon') }}/img/brand/youtube-logo.png">PlayLists Club
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Navigation -->
            @auth()
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#navbar-examples" data-toggle="collapse" role="button" aria-expanded="false">
                        <span class="nav-link-text" style="color: #f4645f;">{{ __('Profile management') }}</span>
                    </a>

                    <div class="collapse" id="navbar-examples">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('profile.edit') }}">
                                    {{ __('My profile') }}
                                </a>
                            </li>
                            @if(auth()->user()->role ==1)
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('user.index') }}">
                                    {{ __('User Management') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                    <a class="nav-link" href="{{ route('manage.ad') }}">
                                        {{ __('Ads management') }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="" data-toggle="modal" data-target = "#advertisement">
                                        {{ __('Add Ads') }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('contact.get')}}">
                                        {{ __('Contact manage') }}
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </li>
            </ul>
        @endauth
            <!-- Heading -->
            <h6 class="navbar-heading text-muted">Categories</h6>
            <!-- Navigation -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('home')}}">
                        <i class="ni ni-like-2 text-primary"></i> {{ __('Most Popular') }}
                    </a>
                </li>
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="#">--}}
                        {{--<i class="ni ni-trophy text-blue"></i> {{ __('Trending') }}--}}
                    {{--</a>--}}
                {{--</li>--}}
                <li class="nav-item">
                    <a class="nav-link" href="{{route('home.newest')}}">
                        <i class="ni ni-button-play text-orange"></i> {{ __('New') }}
                    </a>
                </li>
            </ul>
            <!-- Divider -->
            <hr class="my-3">
            <!-- Heading -->
            <h6 class="navbar-heading text-muted">Top  20 Categories</h6>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                </div>
                <input class="form-control" placeholder="Search categories..." type="text" id="category_search">
            </div>
            <!-- Navigation -->
            <ul class="navbar-nav mb-md-3" id = 'category_box'>
                @foreach($categories as $category)
                <li class="nav-item">
                    <a class="nav-link" href="{{route('home', ['id' => $category->id])}}">
                        <i class="ni ni-tag text-info"></i> {{$category->name}}
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</nav>