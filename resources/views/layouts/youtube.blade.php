<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Argon Dashboard') }}</title>
    <!-- Favicon -->
    <link href="{{ asset('argon') }}/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="{{ asset('argon') }}/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="{{ asset('argon') }}/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <!-- Argon CSS -->
    <link type="text/css" href="{{ asset('argon') }}/css/argon.css?v=1.0.0" rel="stylesheet">
    <link type="text/css" href="{{ asset('argon') }}/css/custom.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('argon') }}/vendor/tag/src/jquery.tagsinput-revisited.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('argon') }}/vendor/toastr/toastr.css" rel="stylesheet">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body class="{{ $class ?? '' }}">
@auth()
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
@endauth

@include('layouts.navbars.sidebar')

<div class="main-content">
    @include('layouts.navbars.navbar')
    @yield('content')
</div>


<script src="{{ asset('argon') }}/vendor/jquery/dist/jquery.min.js"></script>
<script src="{{ asset('argon') }}/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

@stack('js')

<!-- Argon JS -->
<script>
    let ajax_url = '{{route("home.getCategory")}}';
    let action_ajax_url = '{{route('home.action')}}';
    let categories = [
        @if(isset($all_categories))
        @foreach($all_categories as $all_category)
            '{{$all_category}}',
        @endforeach
        @endif
    ];
</script>
<script src="{{ asset('argon') }}/js/argon.js?v=1.0.0"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<script src="{{ asset('argon') }}/vendor/tag/src/jquery.tagsinput-revisited.js"></script>
<script src="{{ asset('argon') }}/vendor/toastr/toastr.js"></script>
<script src="{{ asset('argon') }}/js/custom.js"></script>
<script src="http://www.youtube.com/player_api"></script>
</body>
</html>