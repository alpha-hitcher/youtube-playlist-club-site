$('#category').tagsInput({
    minChars: 0,
    maxChars: null,
    limit: 1,
    validationPattern: null,
    unique: true,
    hide: true,
    delimiter:',',
    placeholder : 'Choose or create your own category to create your playlist',
    'autocomplete': {
        source: categories
    },
});

$('#url').tagsInput({
    minChars: 0,
    maxChars: null,
    limit: null,
    validationPattern: null,
    unique: true,
    hide: true,
    delimiter:',',
    placeholder : 'Put your video URLs here and hit Enter to input another URL.',
});



$("#category_search").on('keyup',function () {

    let key = $(this).val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: ajax_url,
        dataType: 'JSON',
        data:{
            key: key
        },
        method:'PUT',
        success : function (data) {
            let html = '';
            data.forEach(category =>{
                html += '<li class="nav-item"><a class="nav-link" href="'+ajax_url + '/' + category.id+'"><i class="ni ni-tag text-info"></i> ' + category.name + '</a></li>';
            });
            $('#category_box').html(html);
        }
    })
});
$("#playlist_search").on('keyup',function () {



    let key = $(this).val();



    $.ajax({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        },

        url: ajax_url,

        dataType: 'JSON',

        data:{

            key: key

        },

        method:'POST',

        success : function (data) {

            console.log(data);

            let html = '';

            data.forEach(playlist =>{

                html += '<tr>' +

                    '                        <th scope="row">' +

                    '                            <div class="media align-items-center">' +

                    '                                <a href="'+ajax_url+'/playlist/'+playlist['id']+'" class=" mr-3" target="_blank">' +

                    '                                    <img src="'+playlist['top_video']['thumbnail']+'" style="width:120px;height:80px;">' +

                    '                                    <span class="media-body">' +

                    '                                        <span class="mb-0 text-sm">'+playlist['name']+'</span>' +

                    '                                    </span>' +

                    '                                </a>' +

                    '                            </div>' +

                    '                        </th>' +

                    '                        <td><button type="button" class="btn btn-warning btn-sm">'+playlist['category_name']+'</button></td>' +

                    '                        <td>' +

                    '                            <button class="btn btn-icon btn-2 btn-primary" type="button">' +

                    '                                ' +playlist['total_videos'] +

                    '                            </button>' +

                    '                        </td>' +

                    '                        <td>' +

                    '                            <button class="btn btn-icon btn-2 btn-danger action" type="button" data-toggle="tooltip" data-placement="top" title="" data-score="liked:playlist:'+playlist['id']+'" data-original-title="Like" onclick="action(this)">' +

                    '                                👍<i>'+playlist['liked']+'</i>' +

                    '                            </button>' +

                    '                            <button class="btn btn-icon btn-2 btn-info action" type="button" data-toggle="tooltip" data-placement="top" title="" data-score="dislike:playlist:'+playlist['id']+'" data-original-title="Dislike"  onclick="action(this)">' +

                    '                                👎<i>'+playlist['dislike']+'</i>' +

                    '                            </button>' +

                    '                            <button type="button" class="btn btn-primary action" data-toggle="modal" data-target="#comment" data-score="comment:playlist:'+playlist['id']+'"  onclick="action(this)">' +

                    '                                💬<i>'+playlist['comment']+'</i>' +

                    '                            </button>' +

                    '                        </td>' +

                    '                        <td class="text-right">' +

                    '                            <div class="dropdown">' +

                    '                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +

                    '                                    <i class="fas fa-ellipsis-v"></i>' +

                    '                                </a>' +

                    '                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">' +

                    '                                    <a class="dropdown-item share" data-toggle="modal" data-target="#share_link" data-score="'+ajax_url+'/playlist/'+playlist['id']+'" onclick="share(this)"><i class="fa fa-share"></i>Share</a>' +

                    '                                    <a class="dropdown-item action" data-toggle="modal" data-score="favor:playlist:'+playlist['id']+'"  onclick="action(this)"><i class="ni ni-fat-add"></i> Add to Favorite</a>' +

                    '                                </div>' +

                    '                            </div>' +

                    '                        </td>' +

                    '                    </tr>';

            });

            $('#playlist-box').html(html);

        }

    })

});
function action(element) {
    let data = element.getAttribute('data-score');
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: action_ajax_url,
        dataType: 'JSON',
        data:{
            data: data
        },
        method:'POST',
        success : function (data) {
            let form = $(".comment-form");
            let playlist_id = data.selector.split(':')[2];
            form.attr('action', ajax_url + '/playlist/' + playlist_id);
            if (data.key === 'comment') {
                let html= '';
                if (data.comments.length ===0){
                    html = "No comments to show..."
                } else
                    data.comments.forEach(comment => {
                        html += '<div class="comment">' +
                            '                            <div class="avatar-container">' +
                            '                                <a style="vertical-align: middle" class="avatar avatar-md" data-toggle="tooltip" data-original-title="Alexander Smith">' +
                            '                                    <img alt="Image placeholder" src="'+ ajax_url +'/argon/img/avatar/'+comment['user']['avatar']+'" class="rounded-circle">' +
                            '                                </a>' +
                            '                            </div>' +
                            '                            <div class="comment-container">' +comment['content']+ '</div>' +
                            '                            <div class="comment-time">'+ comment['created_at'] +'</div>' +
                            '                        </div>' +
                            '                        <hr class="my-3">';
                    });
                $(".comment-modal-body").html(html);
            }else if (data.key === 'favor') {
                data.content?toastr.success('Added successfully !'): toastr.warning('Already added !');
            }
            else {
                let element = $('[data-score= "'+data.selector+'"] >i' );
                let num = element.text();
                element.text(parseInt(num) + 1);
                toastr.success('Action Success!');
            }
        },
        error : function () {
            toastr.warning('Sorry, You should login for this action');
        }
    })
}
function share(element) {
    $("#share_link_input").val(element.getAttribute('data-score'));
}
function copyLinkToClipboard() {
    let el = $("#share_link_input");
    el.select();
    document.execCommand('copy');
}
function deleteFavor(element) {
    if (confirm('Are you Really?')) {
        element.children('form').submit();
    }
}
function edit(element) {
    let id = element.attr('data-score');
    $("#tab_button").trigger('click');
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: ajax_url + '/getPlaylistAjax',
        dataType: 'JSON',
        data:{
            id: id
        },
        method:'POST',
        success : function (data) {
            let playlist ='';
            $("#playlist_edit_form").attr('action', ajax_url + '/playlist/edit/' + id);
            $("#name_edit").val(data[0].name);
            $('#category_edit').val(data[0]['category_name']);
            data.forEach((video, i) =>{
                playlist += video.url ;
                if (i < data.length - 1) {
                    playlist += ',';
                }
            });
            $("#url_edit").val(playlist);
            $('#url_edit').tagsInput({
                minChars: 0,
                maxChars: null,
                limit: null,
                validationPattern: null,
                unique: true,
                hide: true,
                delimiter:',',
                placeholder : 'Put your video URLs here and hit Enter to input another URL.',

            });

        }

    });

}





