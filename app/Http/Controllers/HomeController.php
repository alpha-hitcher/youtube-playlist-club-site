<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('dashboard');
    }

    public function reindex($category_id = 0)
    {

        $data = DB::table('category')->select('id', 'name','playlist') ->orderBy('name')->limit(8)->get();
        $data1 = array();
        if ($category_id)
            $playlists = DB::table('playlist')->select() ->where('category', $category_id)->where('display_state',1)->orderBy('liked', 'DESC')->limit(10)->get();
        else
            $playlists = DB::table('playlist')->select() ->where('display_state','=',1)->orderBy('liked', 'DESC')->limit(10)->get();
        foreach ($playlists as $playlist)
        {
            $playlist->category_name = DB::table('category') ->where('id', $playlist ->category)->value('name');
            $playlist->top_video = DB::table('videos') ->select()->where('playlist', $playlist->id)->orderBy('liked','DESC')->first() ;
            $playlist->total_videos= DB::table('videos') ->select()->where('playlist', $playlist->id)->count();
            array_push($data1,$playlist);
        }

        $adverse = DB::table('advertisements') -> where('state' , true) ->orderBy('created_at', 'DESC') ->limit(5)->get();

        return view('home')->with(['categories' => $data, 'playlists' =>$data1, 'ads' => $adverse]);
    }

    public function newest()
    {
        $data = DB::table('category')->select('id', 'name','playlist') ->orderBy('name')->limit(8)->get();
        $data1 = array();
        $playlists = DB::table('playlist')->select() ->orderBy('created_at', 'DESC')->limit(10)->get();
        foreach ($playlists as $playlist)
        {
            $playlist->category_name = DB::table('category') ->where('id', $playlist ->category)->value('name');
            $playlist->top_video = DB::table('videos') ->select()->where('playlist', $playlist->id)->orderBy('liked','DESC')->first() ;
            $playlist->total_videos= DB::table('videos') ->select()->where('playlist', $playlist->id)->count();
            array_push($data1,$playlist);
        }
        $ads = DB::table('advertisements')->select() ->where('state' , true) ->orderBy('created_at')->get();
        return view('home')->with(['categories' => $data, 'playlists' =>$data1, 'ads' => $ads]);
    }

    public function video($playlist)
    {
        $data = DB::table('category')->select('id', 'name','playlist') ->orderBy('name')->limit(8)->get();
        $data1 = array();
        $videos = DB::table('videos')->select() ->where('playlist', $playlist)->orderBy('liked','DESC')->get();
        foreach ($videos as $video)
        {
            $video->playlist_name = DB::table('playlist') ->where('id', $playlist)->value('name');
            $video->playlist_id = DB::table('playlist') ->where('id', $playlist)->value('id');
            array_push($data1,$video);
        }

        $ads = DB::table('advertisements')->select() ->where('state' , true) ->orderBy('created_at')->get();

        return view('video')->with(['categories' => $data, 'videos' =>$data1, 'ads' => $ads]);
    }

    public function getCategory(Request $request)
    {
        $key = $request ->get('key');
        $result = DB::table('category')->select('id', 'name','playlist')->where('name', 'like', $key.'%')->orderBy('name')->limit(8)->get();
        return json_encode($result);
    }


    public function getPlaylist(Request $request)
    {
        $data = array();
        $key = $request ->post('key');
        $playlists = DB::table('playlist')->select()->where('name', 'like', $key.'%')->orderBy('liked')->limit(10)->get();
        foreach ($playlists as $playlist)
        {
            $playlist->category_name = DB::table('category') ->where('id', $playlist ->category)->value('name');
            $playlist->top_video = DB::table('videos') ->select()->where('playlist', $playlist->id)->orderBy('liked','DESC')->first() ;
            $playlist->total_videos= DB::table('videos') ->select()->where('playlist', $playlist->id)->count();
            array_push($data,$playlist);
        }
        return json_encode($data);
    }

    public function action(Request $request)
    {
        $action_type = explode(':',$request->get('data'))[0];

        if ($action_type !== 'comment' && is_null(auth()->user())){
            return;
        }

        $data = array();

        $action_table = explode(':',$request->get('data'))[1];
        $action_target_id = (int)explode(':',$request->get('data'))[2];
        switch ($action_type){
            case 'liked':
                $state = $this->checkPrevAction($action_table, 'liked_by', $action_target_id);
                if ($state === false){
                    DB::table($action_table)->where('id', $action_target_id) ->increment('liked');
                    $data['content'] = false;
                }else {
                    $data['content'] = true;
                }
                $data['selector'] = $request->post('data');
                $data['key'] = 'liked';
                return json_encode($data);
                break;
            case 'dislike':
                $state = $this->checkPrevAction($action_table, 'disliked_by', $action_target_id);
                if ($state === false){
                    DB::table($action_table)->where('id', $action_target_id) ->increment('dislike');
                    $data['content'] = false;
                }else {
                    $data['content'] = true;
                }
                $data['selector'] = $request->post('data');
                $data['key'] = 'dislike';
                return json_encode($data);
                break;
            case 'comment':
                $data['comments'] = array();
                $action_table==='playlist'?$parent = 1:$parent = 2;
                $comments = DB::table('comment') ->select()->where('playlist', $action_target_id)->where('parent', $parent)->orderBy('created_at', 'DESC')->get();
                foreach ($comments as $comment)
                {
                    $comment->user = DB::table('users')->select()->where('id', $comment->written_by)->get()->first();
                    array_push($data['comments'],$comment);
                }
                $data['selector'] = $request->post('data');
                $data['key'] = 'comment';
                return json_encode($data);
                break;
            case 'favor':
                $data['key'] = 'favor';
                $data['selector'] = $request->post('data');
                $favor_list_string = DB::table('users') ->select('favor') ->where('id', auth()->user()->id)->get()->first()->favor;
                $favor_list = explode(',', $favor_list_string);
                if (in_array($action_target_id, $favor_list)){
                    $data['content'] = false;
                }else {
                    $data['content'] = true;
                    DB::table('users') ->where(['id'=> auth() ->user()->id]) ->update(['favor'=> $favor_list_string.$action_target_id.',']);
                }
                return json_encode($data);
        }

    }

    public function adverse()
    {
        $data = DB::table('category')->select('id', 'name','playlist') ->orderBy('name')->limit(8)->get();
        $ads = DB::table('advertisements')->select() ->orderBy('created_at')->get();
        return view('adverse', ['ads' => $ads, 'categories' => $data]);
    }

    public function publishAds(Request $request)
    {
        if ($request->get('state'))
            DB::table('advertisements') ->where('id',$request ->get('id')) ->update(['state' =>true]);
        else
            DB::table('advertisements') ->where('id',$request ->get('id')) ->update(['state' =>false]);
        return back() ->withStatus('success !');
    }

    //check liked or disliked before
    private function checkPrevAction($table, $type, $id)
    {
        $string = DB::table($table) ->select($type) -> where('id', $id) ->get() ->first();
        $state =  strpos($string->$type,','.auth()->user()->id.',');
        if ($state === false){
            DB::table($table) ->where('id', $id)->update([$type => $string->$type.auth()->user()->id.',']);
        }
        return $state;
    }


    public function contact(Request $request)
    {
        DB::table('contact') ->insert(['fullname' => $request->post('fullname'), 'email' => $request->post('email'),
            'subject' => $request->post('subject')]);
        return back()->withStatus('Success!');
    }

    public function getContact()
    {
        $categories = DB::table('category')->select('id', 'name','playlist') ->orderBy('name')->limit(8)->get();
        $contacts = DB::table('contact') ->select()->orderBy('state', 'DESC')->get();
        return view('contact',['contacts' => $contacts, 'categories' => $categories]);
    }

    public function checkContact(Request $request)
    {
        if ($request->get('state'))
            DB::table('contact') ->where('id',$request ->get('id')) ->update(['state' =>true]);
        else
            DB::table('contact') ->where('id',$request ->get('id')) ->update(['state' =>false]);
        return back() ->withStatus('success !');
    }

}
