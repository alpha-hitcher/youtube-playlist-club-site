<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use http\Env\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {

        $sidebar = DB::table('category')->select('id', 'name','playlist') ->orderBy('name')->limit(8)->get();

        $array = array();
        $all_categories = DB::table('category')->select('id', 'name','playlist') ->orderBy('name')->get();
        foreach ($all_categories as $all_category)
        {
            array_push($array, $all_category->name);
        }
        $data = array();// for my play list

        $id = auth() ->user()->id;
        $playlists = DB::table('playlist') -> select() ->where('created_by', $id)->get();

        foreach ($playlists as $playlist)
        {
            $playlist->category_name = DB::table('category') ->where('id', $playlist ->category)->value('name');
            $playlist->top_video = DB::table('videos') ->select()->where('playlist', $playlist->id)->orderBy('liked','DESC')->first() ;
            $playlist->total_videos= DB::table('videos') ->select()->where('playlist', $playlist->id)->count();
            array_push($data,$playlist);
        }

        $data2 = array();// for my favor play list

        $favorIDs = explode(',',rtrim(ltrim(DB::table('users')->where('id', $id)->value('favor'),','),','));
        if ($favorIDs[0] !== "")
        {
            foreach ($favorIDs as $favorID)
            {
                $favor_list = DB::table('playlist') ->select()->where('id',(int)$favorID)->get()->first();
                $favor_list->category_name = DB::table('category') ->where('id', $favor_list ->category)->value('name');
                $favor_list->top_video = DB::table('videos') ->select()->where('playlist', $favor_list->id)->orderBy('liked','DESC')->first() ;
                $favor_list->total_videos= DB::table('videos') ->select()->where('playlist', $favor_list->id)->count();
                array_push($data2, $favor_list);
            }
        }
        return view('profile.edit', ['playlists' => $data, 'favors' => $data2, 'categories' => $sidebar, 'all_categories' =>$array]);
    }

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProfileRequest $request)
    {
        auth()->user()->update($request->all());

        return back()->withStatus(__('Profile successfully updated.'));
    }

    /**
     * Change the password
     *
     * @param  \App\Http\Requests\PasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(PasswordRequest $request)
    {
        auth()->user()->update(['password' => Hash::make($request->get('password'))]);

        return back()->withPasswordStatus(__('Password successfully updated.'));
    }


    public function upload(HttpRequest $request)
    {
        $current = DB::table('users') ->where('id' , auth()->user()->id) ->value('avatar');
        if ($current !== 'default.jpg')
            $result = Storage::delete('public/image/avatar/'.$current);

        $path = explode('//', $request ->file('profile_image') ->store('public/image/avatar/'))[1];
        DB::table('users') ->where('id', auth()->user()->id) ->update(['avatar' =>$path]);
        return back()->withStatus('Success!');
    }
}
