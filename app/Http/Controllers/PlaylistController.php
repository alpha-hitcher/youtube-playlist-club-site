<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlaylistRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PlaylistController extends Controller
{
    public function index()
    {

    }

    public function store(PlaylistRequest $request)
    {

        $category = $request ->get('category');
        $item = DB::table('category') ->where('name',$category)->count();

       if (!$item)
            DB::table('category') ->insert(['name' =>$category, 'playlist' => 1, 'created_at' =>now(), 'updated_at' =>now()]);
       else
           DB::table('category')->where('name', $category)->increment('playlist');

       $category_id = DB::table('category') ->where('name', $category) ->value('id');


       $name = $request ->get('name');

       $creator = auth()->user()->id;

       DB::table('playlist') ->insert(['name' =>$name, 'created_by' =>$creator, 'category' => $category_id, 'created_at' =>now(), 'updated_at' =>now()]);

       $playlist_id = DB::table('playlist') ->where('name', $name) ->value('id');

       $urls = $request ->get('url');
       $urls = explode(',',$urls);

       foreach ($urls as $url)
       {
           $data = $this -> get_youtube_data($url);
           $embed_code = $data['embed_code'];
           $thumbnail = $data['thumbnail'];
           $video_id = $data['id'];
           DB::table('videos') ->insert(['url' => $url, 'video_id' => $video_id,'embed_code' =>$embed_code, 'thumbnail' => $thumbnail, 'playlist' => $playlist_id, 'created_at'=>now(), 'updated_at' => now()]);
       }

        return back()->withStatus(__('Playlist created successfully.'));
    }

    public function changeState(Request $request)
    {

        if ($request->get('state'))
        DB::table('playlist')->where('id',$request->get('id'))->update(['display_state' =>true]) ;
        else
            DB::table('playlist')->where('id',$request->get('id'))->update(['display_state' =>false]) ;

        return back()->withStatus(__('Changed successfully.'));

    }

    public function comment(Request $request, $playlist_id)
    {
        $request ->get('type') ==='videos'?$parent= 2:$parent = 1;
         DB::table('comment') ->insert(['content'=>$request->get('comment'), 'written_by' => auth()->user()->id, 'playlist' => $playlist_id, 'parent' => $parent,'created_at' =>now(), 'updated_at' =>now()]);
         if ($parent === 2){
             DB::table('videos')->where('id', $playlist_id)->increment('comment');
         }else {
             DB::table('playlist')->where('id', $playlist_id)->increment('comment');
         }
         return back()->withStatus('success');
    }

    public function deleteFavor($playlist_id)
    {
        $string = DB::table('users') ->where('id', auth()->user()->id) ->value('favor');

        $update = str_replace(','.$playlist_id.',',',',$string);

        DB::table('users') ->where('id', auth()->user()->id)->update(['favor' => $update]);

        return back() ->withStatus('Playlist deleted from favor list.');
    }

    public function getPlaylist(Request $request)
    {
        $result = DB::table('playlist')->where('playlist.id', $request->post('id'))
            ->join('category','playlist.category','=', 'category.id')
            ->join('videos','playlist.id','=','videos.playlist')
            ->select('playlist.*','category.name as category_name' ,'videos.url')
            ->get();
        return json_encode($result);
    }

    public function editPlaylist(Request $request, $playlist_id)
    {
        DB::table('playlist') ->where('id', $playlist_id) ->update(['name' => $request->post('name')]);
        $origins = DB::table('videos')->select('url')->where('playlist',$playlist_id)->get();
        $new = $request->post('url');
        foreach ($origins as $origin)
        {
            if (strpos($new, $origin->url) === false)
            {
                DB::table('videos')->where('url', $origin->url)->delete();
            }
        }
        $urls = explode(',', $new);
        foreach ($urls as $url)
        {
            if (count(DB::table('videos') ->where('url', $url)->where('playlist', $playlist_id) ->get()) === 0){
                $data = $this -> get_youtube_data($url);
                $embed_code = $data['html'];
                $thumbnail = $data['thumbnail_url'];
                DB::table('videos') ->insert(['url' => $url, 'embed_code' =>$embed_code, 'thumbnail' => $thumbnail, 'playlist' => $playlist_id, 'created_at'=>now(), 'updated_at' => now()]);
            }
        }
        return back()->withStatus('Updated Successfully !');
    }

    public function adverse(Request $request)
    {

        $url = $request ->post('url');

        $data = $this -> get_youtube_data($url);
        $thumbnail = $data['thumbnail'];
        $embed_code = $data['embed_code'];

        DB::table('advertisements') ->insert(['url' => $url, 'embed_code' => $embed_code, 'thumbnail' => $thumbnail,'created_at' =>now(), 'updated_at' =>now()]);

        return back() ->withStatus('success');


    }

    private function get_youtube_data($url){
        $data = array();

        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
        $youtube_id = $match[1];

        $youtube = "http://www.youtube.com/oembed?url=". $url ."&format=json";

        $curl = curl_init($youtube);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($curl);
        curl_close($curl);
        $return = json_decode($return , true);

        $data['thumbnail'] = $return['thumbnail_url'];
        $data['id']= $youtube_id;

        $embed_code = $return['html'];

        $embed_code = str_replace('<iframe', '<iframe id="'.$youtube_id.'"', $embed_code);
        $embed_code = str_replace('feature=oembed', 'enablejsapi=1', $embed_code);
        $data['embed_code'] = $embed_code;

        return $data;
    }
}
